'use strict';

const activeClass  =   'is-active';
const fixedClass   =   'is-fixed';
const focusClass   =   'is-focused';
const hoverClass   =   'is-hover';
const disabledClass =  'is-disabled';
const visibleClass =   'is-visible';
const expandedClass =  'is-expanded';
const selectedClass =  'is-selected';
const collapsedClass = 'is-collapsed';
const scrolledClass =  'is-scrolled';
const headerFixedClass = 'header-is-fixed';
const lockedScrollClass = 'scroll-is-locked';
const searchVisibleClass = 'search-is-visible';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';
const asideVisibleClass = 'aside-is-visible';

const $header = $('[data-component="header"]');
const $body = $('.body');
const $main = $('.main');

const bpSM = 520;
const bpMD = 768;
const bpLG = 1024;
const bpXL = 1280;
const bp2Xl = 1440;
const bp3XL = 1920;
/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $toggle.closest('[data-component="accordion"]');
        let $item = $toggle.closest('[data-element="accordion-item"]');
        let $content = $item.find('[data-element="accordion-content"]');

        if (!$item.hasClass(collapsedClass)) {
            $item.siblings().addClass(collapsedClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp(150);
            $content.slideDown(500);
            $item.removeClass(collapsedClass);
        } else {
            $content.slideUp(150);
            $item.addClass(collapsedClass);
        }
    });
});

/*
  Consider using these polyfills to broaden browser support:
    — https://www.npmjs.com/package/classlist-polyfill
    — https://www.npmjs.com/package/nodelist-foreach-polyfill
*/
let $adaptibleBlocks = document.querySelectorAll('[data-component="adaptible-tags"]');

Array.from($adaptibleBlocks).forEach(function(container) {
    let containerId = container.dataset.id;
    let containerBreakpoint = +container.dataset.breakpoint;
    let windowWidth = window.innerWidth;

    if (windowWidth >= containerBreakpoint) {
        return false;
    }

    let tagClass = container.dataset.tagClass;
    let tippyPlacement = container.dataset.tippyPlacement;
    let primary = container;
    let primaryItems = primary.querySelectorAll('[data-element="adaptible-item"]');
    container.classList.add("--jsfied");

    // insert "more" button and duplicate the list
    primary.insertAdjacentHTML(
    "beforeend",
    `
      <li class="ta__item col" data-element="adaptible-more">
        <button class="${tagClass}" data-template="adaptible-tags-template-${containerId}" data-element="popover-template-trigger" data-tippy-placement="${tippyPlacement}" data-tippy-maxWidth="335">&hellip;</button>
        <div hidden>
          <div id="adaptible-tags-template-${containerId}">
            <ul class="tags tags--default row row--xs" data-element="adaptible-secondary">
              ${primary.innerHTML}
            </ul>
          </div>
        </div>
      </li>
    `);

    let secondary = container.querySelector('[data-element="adaptible-secondary"]');
    let secondaryItems = secondary.querySelectorAll('li');
    let allItems = container.querySelectorAll('[data-element="adaptible-item"]');
    let moreElement = primary.querySelector('[data-element="adaptible-more"]');

    let secondaryTags = secondary.querySelectorAll('.tag');
    secondaryTags.forEach((tag, i) => {
        tag.classList.remove('tag--sm');
    });

    // adapt tabs

    const doAdapt = () => {
        // reveal all items for the calculation
        allItems.forEach(item => {
            item.classList.remove("hidden");
        });

        // hide items that won't fit in the Primary
        let stopWidth = moreElement.offsetWidth;
        let hiddenItems = [];
        const primaryWidth = primary.offsetWidth;
        primaryItems.forEach((item, i) => {
            if (primaryWidth >= stopWidth + item.offsetWidth) {
                stopWidth += item.offsetWidth;
            } else {
                item.classList.add("hidden");
                hiddenItems.push(i);
            }
        });

        // toggle the visibility of More button and items in Secondary
        if (!hiddenItems.length) {
            moreElement.classList.add("hidden");
        } else {
            secondaryItems.forEach((item, i) => {
                if (!hiddenItems.includes(i)) {
                    item.classList.add("hidden");
                }
            });
        }
    };

    doAdapt(); // adapt immediately on load
    window.addEventListener("resize", doAdapt); // adapt on window resize
    window.addEventListener("orientationchange", doAdapt); // adapt on window resize
});
// Amount form
$(document).ready(function() {
    $(document).on('click', function(e) {
        var target = $(e.target);
        var amountForm = "";

        if (e.target.dataset.component == "amount-form") {
            amountForm = target;
        } else if (target.parents('[data-component="amount-form"]').length) {
            amountForm = target.parents('[data-component="amount-form"]');
        }

        if (!amountForm) return;

        var
        amountData = amountForm.find('[data-element="amount-data"]'),
        amountValue = amountData.html(),
        amountValueNumber = +amountValue,
        amountMin = +amountData.attr('data-min')  || 0,
        amountMax = +amountData.attr('data-max'),
        amountStep = +amountData.attr('data-step') || 1,
        amountNewValue = 0;

        //amountData.focus().val('').val(amountValue);

        if (target.attr('data-element') === 'inc-control' || target.attr('data-element') === 'dec-control') {
            if (target.attr('data-element') === 'inc-control') {
                amountForm.find('[data-element="dec-control"]').prop('disabled', false);
                if (amountMax && amountValueNumber >= amountMax) {
                    amountNewValue = amountValueNumber;
                    target.prop('disabled', true);
                } else {
                    amountNewValue = amountValueNumber + amountStep;
                }
                if (amountNewValue >= amountMax) {
                    target.prop('disabled', true);
                }
            } else if (target.attr('data-element') === 'dec-control') {
                amountForm.find('[data-element="inc-control"]').prop('disabled', false);
                if (amountValueNumber <= amountMin) {
                    amountNewValue = amountValueNumber;
                } else {
                    amountNewValue = amountValueNumber - amountStep;
                }
                if (amountNewValue <= amountMin) {
                    target.prop('disabled', true);
                }
            }
            if (Number.isInteger(amountStep)) {
                amountData.html(amountNewValue);
            } else {
                amountData.html(amountNewValue.toFixed(1));
            }
            //amountData.blur();
        } else {
            //amountData.focus().val('').val(amountValue);
            amountData.html(amountValue);
        }
    });
});
/* Arrivals Slider */
(function() {
    let $arrivalsSlider = new Swiper('[data-slider="arrivals"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.arrivals__next',
            prevEl: '.arrivals__prev',
        }
    });
})();
/* Benefits Slider */
(function() {
    let $benefitsSlider = new Swiper('[data-slider="benefits"]', {
        freeMode: true,
        slidesPerView: 'auto',
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.benefits__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.benefits__next',
            prevEl: '.benefits__prev',
        },
        breakpoints: {
            320: {
                spaceBetween: 32,
            },
            768: {
                spaceBetween: 40,
            },
        },
    });

    let $benefitsExtendedSlider = new Swiper('[data-slider="benefits-extended"]', {
        freeMode: true,
        slidesPerView: 'auto',
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.benefits-extended__next',
            prevEl: '.benefits-extended__prev',
        },
        breakpoints: {
            320: {
                spaceBetween: 24,
            },
            768: {
                spaceBetween: 30,
            },
        },
    });
})();
/* Brands Slider */
(function() {
    let $brandsSlider = new Swiper('[data-slider="brands"]', {
        slidesPerView: 4,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.brands__pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
            },
            480: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
                //slidesPerColumn: 2,
                //slidesPerColumnFill: 'row',
            },
            1280: {
                slidesPerView: 4,
            },
        },
    });
})();
/* Details with equal name width */
$(document).ready(function() {
    catalogSliderAdjustment();
    $(window).on('orientationchange resize', function() {
        catalogSliderAdjustment();
    });

    function catalogSliderAdjustment() {
        let $catalogSlider = $('[data-slider="catalog"]');

        $catalogSlider.each(function (index, item) {
            let $slider = $(item);
            let $slides = $slider.find('[data-element="catalog-slide"]');
            let $slidesCards = $slider.find('[data-element="catalog-slide"] .item-card');

            let tallestSlide = Math.max.apply(Math, $slidesCards.map(function(index, el) {
                return $(el)[0].scrollHeight;
            }));

            $slides.css('height', tallestSlide + 'px');

        });
    };
});
/* Catalog Slider */
(function() {
    let $catalogSliders = Array.from(document.querySelectorAll('[data-slider="catalog"]'));

    $catalogSliders.forEach((slider) => {
        let nextButton = slider.closest('.catalog').querySelector('.swiper-button-next');
        let prevButton = slider.closest('.catalog').querySelector('.swiper-button-prev');

        let $catalogSlider = new Swiper(slider, {
            slidesPerView: 'auto',
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
        });
    });
})();
/* Catalog Tags Slider */
(function() {
    let $catalogTagsSlider = new Swiper('[data-slider="catalog-tags"]', {
        slidesPerView: 'auto',
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
})();
/* Categories Slider */
(function() {
    let $categoriesSlider = new Swiper('[data-slider="categories"]', {
        slidesPerView: 5,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.categories__menu-pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            576: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 3,
            },
            1024: {
                slidesPerView: 4,
            },
            1280: {
                slidesPerView: 5,
            }
        }
    });
})();
/* Categories Tags Slider */
$(document).ready(function(){
    let $categoriesTagsSlider = $('[data-slider="categories-tags"]');

    if ($categoriesTagsSlider.length === 0) {
        return false;
    }

    $categoriesTagsSlider.slick({
        mobileFirst: true,
        infinite: false,
        slidesPerRow: 1,
        rows: 4,
        dots: true,
        prevArrow: $('.categories__tags-prev'),
        nextArrow: $('.categories__tags-next'),
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    dots: false,
                    autoSlidesToShow: true,
                    variableWidth: true,
                    slidesPerRow: 4,
                    rows: 2,
                }
            }
        ]
    });

    function isInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function resizeSlider() {
        let winWidth = $(window).width();
        let $categoriesTagsSliderList  = $categoriesTagsSlider.find('.slick-list');

        if (winWidth >= bpMD) {
            let $categoriesTagsSliderPrev  = $('.categories__tags-prev');
            let $categoriesTagsSliderNext  = $('.categories__tags-next');
            let $categoriesTagsSliderSlide = $categoriesTagsSlider.find('.slick-slide');
            let sliderListWidth,
                slideWidth,
                leftOffset;

            sliderListWidth = $categoriesTagsSliderList.width();
            slideWidth = $categoriesTagsSliderSlide.width();

            if (isInViewport($categoriesTagsSliderSlide[0])) {
                $categoriesTagsSliderPrev[0].style.setProperty('display', 'none', 'important');
                $categoriesTagsSliderNext[0].style.setProperty('display', 'none', 'important');
            } else {
                $categoriesTagsSliderPrev[0].style.setProperty('display', '');
                $categoriesTagsSliderNext[0].style.setProperty('display', '');
            }

            $categoriesTagsSliderList.css('left', 0);

            $categoriesTagsSliderNext.on('click', function(e) {
                leftOffset = slideWidth - sliderListWidth;
                $categoriesTagsSliderList.css("left", '-'+ leftOffset + 'px');
            });

            $categoriesTagsSliderPrev.on('click', function(e) {
                $categoriesTagsSliderList.css("left", 0);
            });
        } else {
            $categoriesTagsSliderList.css('left', '');
        }
    }

    resizeSlider();

    $(window).on('orientationchange resize', function() {
        $categoriesTagsSlider.slick('resize');
        resizeSlider();
    });
});
/* Compare slider */
$(document).ready(function(){
    let $compareSlider = $('[data-slider="compare"]');
    let $compareCounter = $('[data-element="compare-counter"]');

    $compareSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        var i = (currentSlide ? currentSlide : 0) + 1;
        $compareCounter.text(i + ' из ' + slick.slideCount);
    });

    $compareSlider.slick({
        infinite: false,
        autoSlidesToShow: true,
        variableWidth: true,
        asNavFor: '[data-slider="compare-props"]',
        prevArrow: $('.compare__slider-prev'),
        nextArrow: $('.compare__slider-next'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                }
            }
        ]
    });

    let $comparePropsSlider = $('[data-slider="compare-props"]');
    $comparePropsSlider.slick({
        infinite: false,
        autoSlidesToShow: true,
        variableWidth: true,
        draggable: false,
        touchMove: false,
        swipe: false,
        arrows: false,
        asNavFor: '[data-slider="compare"]',
    });

    $(window).on('orientationchange resize', function() {
        $compareSlider.slick('resize');
        $comparePropsSlider.slick('resize');
    });
});
/* Datepicker */
document.addEventListener('DOMContentLoaded', function() {
    let $datePickers = document.querySelectorAll('[data-element="datepicker"]');

    if ($datePickers) {
        for (var i = 0; i < $datePickers.length; i++) {
            let picker = datepicker($datePickers[i], {
                position: 'bl',
                startDay: 1,
                showAllDates: true,
                customDays: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                customOverlayMonths: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Aвг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                customMonths: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Aвгуст', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                overlayButton: 'Выбрать',
                overlayPlaceholder: 'Год',
                formatter: (input, date, instance) => {
                    const value = date.toLocaleDateString();
                    input.value = value;
                },
                onSelect: (instance, date) => {
                    $(instance.el).parsley().validate();
                },
            });
        }
    }
});
/* Diagnostics Slider */
(function() {
    let $diagnosticsSlider = new Swiper('[data-slider="diagnostics"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.diagnostics__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.diagnostics__next',
            prevEl: '.diagnostics__prev',
        }
    });
})();
// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Поле обязательно для заполнения",
    required:       "Поле обязательно для заполнения",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    $(function() {
        let $parsleyForm = $('[data-parsley-validate]');
        let $parsleyFormSubmit = $parsleyForm.find('input[type="submit"]');

        $parsleyForm.parsley({
            excluded: "[disabled], :hidden",
            errorClass: 'has-error',
            successClass: 'has-success',
            errorsWrapper: '<div class="form__errors-list"></div>',
            errorTemplate: '<div class="form__error"></div>',
            errorsContainer (field) {
                return field.$element.parent().closest('.form__input');
            },
            classHandler (field) {
                const $parent = field.$element.closest('.form__input');
                if ($parent.length) return $parent;

                return $parent;
            }
        });
    });

});
/* In cart Slider */
//(function() {
    let $inCartSlider = new Swiper('[data-slider="in-cart"]', {
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.in-cart__slider-pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                slidesPerColumn: 2,
                slidesPerColumnFill: 'row',
                spaceBetween: 8
            },
            1280: {
                slidesPerView: 3,
                slidesPerColumn: 1,
                spaceBetween: 24
            },
        },
    });
//})();
/* Masked input */
$(document).ready(function(){
    $(":input[data-inputmask]").inputmask({
        showMaskOnHover: false,
    });

    /* Masked input fix For parsley validation */
    $(document).on('keypress', function(evt) {
        if(evt.isDefaultPrevented()) {
            // Assume that's because of maskedInput
            // See https://github.com/guillaumepotier/Parsley.js/issues/1076
            $(evt.target).trigger('input');
        }
    });
});
/* Menu Slider */
$(document).ready(function(){
    var $menuSlider = $('[data-slider="menu"]');

    $menuSlider.slick({
        mobileFirst: true,
        arrows: false,
        dots: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 699,
                settings: {
                    slidesToShow: 2,
                } 
            },
            {
                breakpoint: 767,
                settings: "unslick"
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $menuSlider.slick('resize');
    });
});

$(document).ready(function(){
    fixedHeightCheck();
    $(window).on('orientationchange resize', function() {
        fixedHeightCheck();
    });

    function fixedHeightCheck() {
        let $mobileFixedHeight = $('[data-component="mobile-fixed-height"]');

        $mobileFixedHeight.each(function (index, item) {
            let $component = $(item);
            let windowWidth = $(window).width();

            if (windowWidth < bpMD) {
                let $content = $component.find('[data-element="mobile-fixed-height-content"]');
                let $toggle = $component.find('[data-element="mobile-fixed-height-toggle"]');
                let contentHeight = $content.height();

                if (contentHeight >= 252) {
                    let toggleHtml = `
                        <a href="#" class="fixed-height-block__toggle link-pseudo" data-text-on="Развернуть" data-text-off="Свернуть" data-element="mobile-fixed-height-toggle">Развернуть</a>
                    `;

                    $component.addClass(collapsedClass);

                    if ($toggle.length === 0) {
                        $component.append(toggleHtml);
                    }
                } else {
                    $component.removeClass(collapsedClass);
                    $component.find('[data-element="mobile-fixed-height-toggle"]').remove();
                }
            } else {
                $component.removeClass(collapsedClass);
                $component.find('[data-element="mobile-fixed-height-toggle"]').remove();
            }
        });
    }

    $(document).on('click', '[data-element="mobile-fixed-height-toggle"]', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleTextOn = $toggle.attr('data-text-on');
        let toggleTextOff = $toggle.attr('data-text-off');
        let $component = $toggle.closest('[data-component="mobile-fixed-height"]');

        $toggle.toggleText(toggleTextOn, toggleTextOff);
        $component.toggleClass(collapsedClass);

        /*$('html, body').animate({
            scrollTop: $component.offset().top - 80
        }, 500);*/
    });
});

/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuTrigger = $('[data-element="mobile-menu-trigger"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu() {
    $mobileMenu.removeClass(visibleClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(mobileMenuVisibleClass);
       $body.removeClass(lockedScrollClass);
    }
}

function showMobileMenu(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $body.addClass(mobileMenuVisibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ triggerSrc +'"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu();
        } else {
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
            showMobileMenu($component);
        }
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        hideMobileMenu();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu();
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuTrigger.has(e.target).length === 0 && !$mobileMenuTrigger.is(e.target)) {
                hideMobileMenu();
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    });
});
/* Modal */
$(function() {
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        callbacks: {
            beforeOpen: function() {
                let effectData = this.st.el.attr('data-effect');

                if (!effectData) {
                    effectData = 'mfp-zoom-in';
                }

                this.st.mainClass = effectData;
            },
            open: function() {
                $('.body').css('overflow', "hidden");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.attr('disabled', true);

                $inCartSlider.update();
            },
            close: function() {
                $('.body').css('overflow', "");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.removeAttr('disabled');
            }
        },
    });


    $('[data-element="control-modal-trigger"]').change(function() {
        let $control = $(this);

        if ($control.prop('checked')) {
            let mfpSrc = $control.attr('data-src');
            let mfpEffect = $control.attr('data-effect');

            $.magnificPopup.open({
                items: {
                    src: mfpSrc
                },
                type: 'inline',
                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,

                midClick: true,
                removalDelay: 300,
                callbacks: {
                    beforeOpen: function() {
                        let effectData = mfpEffect;

                        if (!effectData) {
                            effectData = 'mfp-zoom-in';
                        }

                        this.st.mainClass = effectData;
                    },
                    open: function() {
                        $('.body').css('overflow', "hidden");
                    },
                    close: function() {
                        $('.body').css('overflow', "");
                    }
                },
            });
        };
    });

    /* Gallery Modal */
    $('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});

/* Move a block between it's mobile and desktop placeholders  */
$(document).ready(function(){
    moveBlocks();
    $(window).on('orientationchange resize', function() {
        moveBlocks();
    });

    function moveBlocks() {
        let $movableBlock = $('[data-block="movable-block"]');

        $movableBlock.each(function (index, item) {
            let $block = $(item);
            let blockID = $block.attr('data-id');
            let $blockPlaceholderMobile = $('[data-element="placeholder"][data-device="mobile"][data-id="' + blockID + '"]');
            let $blockPlaceholderTablet = $('[data-element="placeholder"][data-device="tablet"][data-id="' + blockID + '"]');
            let $blockPlaceholderDesktop = $('[data-element="placeholder"][data-device="desktop"][data-id="' + blockID + '"]');
            let blockBreakpoint = $block.attr('data-breakpoint');
            let windowWidth = $(window).width();
            let breakpoint = bpLG;

            if (blockBreakpoint) {
                breakpoint = blockBreakpoint;
            }

            if (windowWidth >= breakpoint) {
                if ($blockPlaceholderDesktop.length === 0) {
                    $block.appendTo($blockPlaceholderTablet);
                } else {
                    $block.appendTo($blockPlaceholderDesktop);
                }
            } else if (windowWidth >= bpMD && windowWidth < bpLG && $blockPlaceholderTablet.length > 0) {
                $block.appendTo($blockPlaceholderTablet);
            } else if (windowWidth < bpMD) {
                $block.appendTo($blockPlaceholderMobile);
            }
        });
    }
});

/* Panel */
$(document).ready(function(){
    let $panelToggle = $('[data-element="panel-toggle"]');

    $panelToggle.on('click', function(e){
        e.preventDefault();

        let $toggle = $(this);
        let $panel = $toggle.closest('[data-component="panel"]');
        let $content = $panel.find('[data-element="panel-content"]');

        $panel.toggleClass(collapsedClass);
    });
});
/* Parallax */
(function() {
    const parallaxScenes = document.querySelectorAll('[data-component="parallax"]');

    if (!parallaxScenes) {
        return false;
    }

    for (const scene of parallaxScenes) {
        let parallaxInstance = new Parallax(scene);
    }
})();
/* Catalog Slider */
(function() {
    let $parametersSlider = new Swiper('[data-slider="parameters"]', {
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.parameters__slider-next',
            prevEl: '.parameters__slider-prev',
        },
    });
})();
/* Perfect scrollbar */
(function() {
    const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

    if (!psContainers) {
        return false;
    }

    for (const container of psContainers) {
        const ps = new PerfectScrollbar(container);
    }
})();

/* Popover */
tippy('[data-element="popover-trigger"]', {
    maxWidth: 333,
    theme: 'light',
    offset: [0, 21],
    allowHTML: true,
    interactive: true,
});

tippy('[data-element="popover-template-trigger"]', {
    maxWidth: 333,
    theme: 'light',
    offset: [-56, 21],
    allowHTML: true,
    appendTo: () => document.body,
    interactive: true,
    content(reference) {
        const id = reference.getAttribute('data-template');
        const template = document.getElementById(id);
        return template.innerHTML;
    },
});
/* Product slider */
//$(function() {
    let $productThumbs = new Swiper('[data-slider="product-thumbs"]', {
        spaceBetween: 12,
        slidesPerView: 3,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });

    let $productSlider = new Swiper('[data-slider="product"]', {
        navigation: {
            nextEl: '.product__slider-next',
            prevEl: '.product__slider-prev',
        },
        thumbs: {
            swiper: $productThumbs,
        },
    });
    //});
jQuery(document).ready(function($){
    let $rangeSlider = $('[data-element="range-slider"]');

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range"]');
        let $rangeInputFrom = $rangeComponent.find('[data-element="range-input-from"]');
        let $rangeInputTo = $rangeComponent.find('[data-element="range-input-to"]');
        let instance;

        $range.ionRangeSlider({
            skin: 'round',
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            min: $range.attr('data-min'),
            max: $range.attr('data-max'),
            step: $range.attr('data-step'),
            onStart: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            },
            onChange: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            }
        });

        instance = $range.data("ionRangeSlider");

        $rangeInputFrom.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val < instance.result.min) {
                val = instance.result.min;
            } else if (val > instance.result.max) {
                val = instance.result.max;
            }

            instance.update({
                from: val
            });
        });

        $rangeInputTo.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val > instance.result.max) {
                val = instance.result.max;
            } else if (val < instance.result.from) {
                val = instance.result.from;
            }

            instance.update({
                to: val
            });
        });
    });
});
/* Reviews Slider */
(function() {
    let $reviewsSlider = new Swiper('[data-slider="reviews"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.reviews__next',
            prevEl: '.reviews__prev',
        }
    });
})();
/* Search */
$(document).ready(function(){
    let $search = $('[data-component="search"]');
    let $searchInput = $('[data-element="search-input"]');
    let $searchClear = $('[data-element="search-clear"]');

    $searchInput.on('input', function(e) {
        $search.fadeIn();
        $search.addClass(activeClass);
    });

    $searchClear.on('click', function(e) {
        e.preventDefault();

        $searchInput.val('');
        $search.removeClass(activeClass);
    });

    /*$(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            $searchContent.hide();
        }
    });

    $(document).on('click', function(e) {
        if ($searchInput.has(e.target).length === 0 && !$searchInput.is(e.target)) {
            $searchContent.hide();
        }
    });*/
});

/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
    };

    $('[data-element="select"]').select2(selectCommonOptions);
});
/* Show More/Less */
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) {
        this.text(t2);
    } else {
        this.text(t1);
    }
    return this;
};

$(document).ready(function(){
    let $showAllToggle = $('[data-element="show-all-toggle"]');

    $showAllToggle.on('click', function(e) {
        e.preventDefault();

        let toggle = $(this);
        let toggleTextOn = toggle.attr('data-text-on');
        let toggleTextOff = toggle.attr('data-text-off');
        let showAllComponent = toggle.closest('[data-component="show-all"]');

        toggle.toggleText(toggleTextOn, toggleTextOff);

        showAllComponent.find('[data-element="hidden-element"]').toggleClass('hidden');
    });
});

/* Slider Block Slider */
(function() {
    let $sliderBlockSlider = new Swiper('[data-slider="slider-block"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.slider-block__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.slider-block__next',
            prevEl: '.slider-block__prev',
        }
    });
})();
/*! smooth-scroll v12.1.5 | (c) 2017 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t="querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype,n={ignore:"[data-scroll-ignore]",header:null,speed:500,offset:0,easing:"easeInOutCubic",customEasing:null,before:function(){},after:function(){}},o=function(){for(var e={},t=0,n=arguments.length;t<n;t++){var o=arguments[t];!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(o)}return e},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},r=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,a=-1,r="",i=n.charCodeAt(0);++a<o;){if(0===(t=n.charCodeAt(a)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===a&&t>=48&&t<=57||1===a&&t>=48&&t<=57&&45===i?r+="\\"+t.toString(16)+" ":r+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(a):"\\"+n.charAt(a)}return"#"+r},i=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},u=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},c=function(e,t,n){var o=0;if(e.offsetParent)do{o+=e.offsetTop,e=e.offsetParent}while(e);return o=Math.max(o-t-n,0)},s=function(e){return e?a(e)+e.offsetTop:0},l=function(t,n,o){o||(t.focus(),document.activeElement.id!==t.id&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},f=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)};return function(a,d){var m,h,g,p,v,b,y,S={};S.cancelScroll=function(){cancelAnimationFrame(y)},S.animateScroll=function(t,a,r){var f=o(m||n,r||{}),d="[object Number]"===Object.prototype.toString.call(t),h=d||!t.tagName?null:t;if(d||h){var g=e.pageYOffset;f.header&&!p&&(p=document.querySelector(f.header)),v||(v=s(p));var b,y,E,I=d?t:c(h,v,parseInt("function"==typeof f.offset?f.offset():f.offset,10)),O=I-g,A=u(),C=0,w=function(n,o){var r=e.pageYOffset;if(n==o||r==o||(g<o&&e.innerHeight+r)>=A)return S.cancelScroll(),l(t,o,d),f.after(t,a),b=null,!0},Q=function(t){b||(b=t),C+=t-b,y=C/parseInt(f.speed,10),y=y>1?1:y,E=g+O*i(f,y),e.scrollTo(0,Math.floor(E)),w(E,I)||(e.requestAnimationFrame(Q),b=t)};0===e.pageYOffset&&e.scrollTo(0,0),f.before(t,a),S.cancelScroll(),e.requestAnimationFrame(Q)}};var E=function(e){h&&(h.id=h.getAttribute("data-scroll-id"),S.animateScroll(h,g),h=null,g=null)},I=function(t){if(!f()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&(g=t.target.closest(a))&&"a"===g.tagName.toLowerCase()&&!t.target.closest(m.ignore)&&g.hostname===e.location.hostname&&g.pathname===e.location.pathname&&/#/.test(g.href)){var n;try{n=r(decodeURIComponent(g.hash))}catch(e){n=r(g.hash)}if("#"===n){t.preventDefault(),h=document.body;var o=h.id?h.id:"smooth-scroll-top";return h.setAttribute("data-scroll-id",o),h.id="",void(e.location.hash.substring(1)===o?E():e.location.hash=o)}h=document.querySelector(n),h&&(h.setAttribute("data-scroll-id",h.id),h.id="",g.hash===e.location.hash&&(t.preventDefault(),E()))}},O=function(e){b||(b=setTimeout((function(){b=null,v=s(p)}),66))};return S.destroy=function(){m&&(document.removeEventListener("click",I,!1),e.removeEventListener("resize",O,!1),S.cancelScroll(),m=null,h=null,g=null,p=null,v=null,b=null,y=null)},S.init=function(a){t&&(S.destroy(),m=o(n,a||{}),p=m.header?document.querySelector(m.header):null,v=s(p),document.addEventListener("click",I,!1),e.addEventListener("hashchange",E,!1),p&&e.addEventListener("resize",O,!1))},S.init(d),S}}));

var scroll = new SmoothScroll('[data-scroll]', {
	speed: 1000,
	easing: 'easeInOutCubic',
    header: '[data-component="header"]'
});
/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');
    let $supportComplete = $('[data-element="support-complete"]');

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $toggle.closest('[data-component="accordion"]');
        let $indicator = $component.closest('.has-indicator');
        let $item = $toggle.closest('[data-element="accordion-item"]');
        let $content = $item.find('[data-element="accordion-content"]');

        if ($item.hasClass(collapsedClass)) {
            $item.siblings().addClass(collapsedClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp(150);
            $content.slideDown(500);
            $item.removeClass(collapsedClass);

            if ($component.hasClass('support__faq') && $indicator.length > 0) {
                $indicator.removeClass(disabledClass).removeClass(selectedClass);
                $indicator.prev().removeClass(disabledClass).addClass(selectedClass);
                $indicator.next().addClass(disabledClass).removeClass(selectedClass);
            }
        } else {
            $content.slideUp(150);
            $item.addClass(collapsedClass);

            if ($component.hasClass('support__faq') && $indicator.length > 0) {
                $indicator.addClass(disabledClass).removeClass(selectedClass);
                $indicator.prev().removeClass(selectedClass);
            }
        }
    });

    $supportComplete.on('click', function(e) {
        e.preventDefault();

        let $indicator = $('.has-indicator');
        let $indicatorLast = $('.has-indicator:last-child');
        let $contacts = $('.has-indicator:last-child .' + disabledClass);

        $indicator.removeClass(disabledClass).addClass(selectedClass);

        $contacts.each(function (index, item) {
            if ( index !== 1) {
                $(item).removeClass(disabledClass)
            }

            $('html, body').animate({
                scrollTop: $indicatorLast.offset().top - 200
            }, 500);
        });
    });
});

/* Panel on mobile, tabs on desktop */
$(document).ready(function(){
    let $panelTabsToggle = $('[data-component="panel-tabs"] [data-element="panel-tabs-toggle"]');
    let $panelTabs = $('[data-component="panel-tabs"]');

    $panelTabsToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleId = $toggle.attr('href');
        let $component = $toggle.closest('[data-component="panel-tabs"]');
        let $tabActive = $component.find('[data-element="panel-tabs-tab"][data-id="' + toggleId.slice(1) +'"]');
        let $tabs = $component.find('[data-element="panel-tabs-tab"]');
        let $toggles = $component.find('[data-element="panel-tabs-toggle"]');
        let $indicator = $toggle.closest('.has-indicator');
        let $faqItems = $tabActive.find('[data-element="accordion-item"]');
        let windowWidth = $(window).width();

        if ($toggle.hasClass(activeClass)) {
            $toggle.removeClass(activeClass);
            $tabActive.removeClass(activeClass);

            if (windowWidth < bpLG) {
                $tabActive.hide();
            }

            if ($indicator.length > 0) {
                $indicator.addClass(disabledClass).removeClass(selectedClass);
            }
        } else {
            $tabs.removeClass(activeClass);
            $toggles.removeClass(activeClass);
            $tabActive.addClass(activeClass);
            $toggle.addClass(activeClass);

            if (windowWidth < bpLG) {
                $tabs.hide();
                $tabActive.slideDown(300);
            }

            if ($indicator.length > 0) {
                $indicator.removeClass(disabledClass).removeClass(selectedClass);
            }
        }

        if ($indicator.length > 0) {
            $indicator.siblings().removeClass(selectedClass).addClass(disabledClass);
        }

        if ($faqItems.length > 0) {
            $faqItems.addClass(collapsedClass);
            $faqItems.find('[data-element="accordion-content"]').hide();
            $faqItems.find('[data-component="accordion"]').addClass(disabledClass);
        }
    });

    panelTabsResponsive();
    $(window).on('orientationchange resize', function() {
      panelTabsResponsive();
    });

    function panelTabsResponsive() {
        $panelTabs.each(function (index, item) {

            let $tabs = $(item);
            let $toggles = $tabs.find('[data-element="panel-tabs-toggle"]');
            let $content = $tabs.find('[data-element="panel-tabs-content"]');

            $toggles.each(function (index, toggle) {
                let $toggle = $(toggle);
                let toggleId = $toggle.attr('href');
                let $tabItem = $toggle.closest('[data-element="panel-tabs-item"]');
                let $tab = $tabs.find('[data-element="panel-tabs-tab"][data-id="' + toggleId.slice(1) +'"]');

                if ($(window).width() < bpLG) {
                    $tab.appendTo($tabItem);
                } else {
                    $tab.appendTo($content);
                    $tab.show();
                }
            });
        });
    }
});
/* Scroll to tabs item */
$(document).ready(function(){
    let $tabTrigger = $('[data-element="tab-trigger"]');

    $tabTrigger.on('click', function(e){
        e.preventDefault();

        let $trigger = $(this);

        let triggerId = $trigger.attr('href');
        let $tabActive = $('[data-element="tabs-tab"][data-id="'+ triggerId.slice(1) +'"]');
        let $toggle = $('[data-element="tabs-toggle"][href="'+ triggerId +'"]');
        let $tabs = $toggle.closest('[data-component="tabs"]');

        if (!($toggle.hasClass(activeClass))) {
            $tabs.find('[data-element="tabs-tab"]').removeClass(activeClass);
            $tabs.find('[data-element="tabs-toggle"]').removeClass(activeClass);
            $tabs.find($tabActive).addClass(activeClass);
            $toggle.addClass(activeClass);
        }

        $('html, body').animate({
            scrollTop: $tabs.offset().top - 200
        }, 500);
    });
});
/* Tabs */
$(document).ready(function(){
    let $tabToggle = $('[data-element="tabs-toggle"]');

    $tabToggle.on('click', function(e) {
        e.stopPropagation();

        let $toggle = $(this);
        let $tabs = $toggle.closest('[data-component="tabs"]');
        let $control = $toggle.find('input');
        let trigger = $toggle.attr('data-trigger');
        let tabID;


        if ($toggle.attr('href')) {
            tabID = $toggle.attr('href').slice(1);
        }

        if ($toggle.attr('data-src')) {
            tabID = $toggle.attr('data-src');
        }

        let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

        if (!($toggle.hasClass(activeClass))) {
            $tabs.find('[data-element="tabs-tab"]').removeClass(activeClass);
            $tabs.find('.' + activeClass +'[data-element="tabs-toggle"]').removeClass(activeClass);
            $tabs.find($tabActive).addClass(activeClass);
            $toggle.addClass(activeClass);
        }

        if (trigger.length > 0) {
            let elBg = $toggle.closest('[data-element="change-bg"]');

            if (trigger === 'list') {
                elBg.addClass('theme-light');
            } else {
                elBg.removeClass('theme-light');
            }
        }
    });
});

/* Tooltip */
tippy('[data-element="tooltip-trigger"]', {
    maxWidth: 324,
    theme: 'light',
    offset: [0, 21],
    interactive: true,
});

  /* Button to top positioning */
  $(function() {
    var buttonToTop = $('[data-component="top-button"]');

    if (!buttonToTop) return false;

    var Toffset = 250;
    var Boffset = $('[data-component="footer"]').outerHeight();

    $(window).scroll(function(){
      if ($(this).scrollTop() > Toffset){
        buttonToTop.addClass("active");
      } else {
        buttonToTop.removeClass("active");
      }
    });
  });