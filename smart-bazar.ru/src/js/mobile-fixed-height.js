$(document).ready(function(){
    fixedHeightCheck();
    $(window).on('orientationchange resize', function() {
        fixedHeightCheck();
    });

    function fixedHeightCheck() {
        let $mobileFixedHeight = $('[data-component="mobile-fixed-height"]');

        $mobileFixedHeight.each(function (index, item) {
            let $component = $(item);
            let windowWidth = $(window).width();

            if (windowWidth < bpMD) {
                let $content = $component.find('[data-element="mobile-fixed-height-content"]');
                let $toggle = $component.find('[data-element="mobile-fixed-height-toggle"]');
                let contentHeight = $content.height();

                if (contentHeight >= 252) {
                    let toggleHtml = `
                        <a href="#" class="fixed-height-block__toggle link-pseudo" data-text-on="Развернуть" data-text-off="Свернуть" data-element="mobile-fixed-height-toggle">Развернуть</a>
                    `;

                    $component.addClass(collapsedClass);

                    if ($toggle.length === 0) {
                        $component.append(toggleHtml);
                    }
                } else {
                    $component.removeClass(collapsedClass);
                    $component.find('[data-element="mobile-fixed-height-toggle"]').remove();
                }
            } else {
                $component.removeClass(collapsedClass);
                $component.find('[data-element="mobile-fixed-height-toggle"]').remove();
            }
        });
    }

    $(document).on('click', '[data-element="mobile-fixed-height-toggle"]', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleTextOn = $toggle.attr('data-text-on');
        let toggleTextOff = $toggle.attr('data-text-off');
        let $component = $toggle.closest('[data-component="mobile-fixed-height"]');

        $toggle.toggleText(toggleTextOn, toggleTextOff);
        $component.toggleClass(collapsedClass);

        /*$('html, body').animate({
            scrollTop: $component.offset().top - 80
        }, 500);*/
    });
});
