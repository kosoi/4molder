/* Benefits Slider */
(function() {
    let $benefitsSlider = new Swiper('[data-slider="benefits"]', {
        freeMode: true,
        slidesPerView: 'auto',
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.benefits__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.benefits__next',
            prevEl: '.benefits__prev',
        },
        breakpoints: {
            320: {
                spaceBetween: 32,
            },
            768: {
                spaceBetween: 40,
            },
        },
    });

    let $benefitsExtendedSlider = new Swiper('[data-slider="benefits-extended"]', {
        freeMode: true,
        slidesPerView: 'auto',
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.benefits-extended__next',
            prevEl: '.benefits-extended__prev',
        },
        breakpoints: {
            320: {
                spaceBetween: 24,
            },
            768: {
                spaceBetween: 30,
            },
        },
    });
})();