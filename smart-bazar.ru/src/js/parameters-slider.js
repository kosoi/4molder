/* Catalog Slider */
(function() {
    let $parametersSlider = new Swiper('[data-slider="parameters"]', {
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.parameters__slider-next',
            prevEl: '.parameters__slider-prev',
        },
    });
})();