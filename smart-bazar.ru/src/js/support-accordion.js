/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');
    let $supportComplete = $('[data-element="support-complete"]');

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $toggle.closest('[data-component="accordion"]');
        let $indicator = $component.closest('.has-indicator');
        let $item = $toggle.closest('[data-element="accordion-item"]');
        let $content = $item.find('[data-element="accordion-content"]');

        if ($item.hasClass(collapsedClass)) {
            $item.siblings().addClass(collapsedClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp(150);
            $content.slideDown(500);
            $item.removeClass(collapsedClass);

            if ($component.hasClass('support__faq') && $indicator.length > 0) {
                $indicator.removeClass(disabledClass).removeClass(selectedClass);
                $indicator.prev().removeClass(disabledClass).addClass(selectedClass);
                $indicator.next().addClass(disabledClass).removeClass(selectedClass);
            }
        } else {
            $content.slideUp(150);
            $item.addClass(collapsedClass);

            if ($component.hasClass('support__faq') && $indicator.length > 0) {
                $indicator.addClass(disabledClass).removeClass(selectedClass);
                $indicator.prev().removeClass(selectedClass);
            }
        }
    });

    $supportComplete.on('click', function(e) {
        e.preventDefault();

        let $indicator = $('.has-indicator');
        let $indicatorLast = $('.has-indicator:last-child');
        let $contacts = $('.has-indicator:last-child .' + disabledClass);

        $indicator.removeClass(disabledClass).addClass(selectedClass);

        $contacts.each(function (index, item) {
            if ( index !== 1) {
                $(item).removeClass(disabledClass)
            }

            $('html, body').animate({
                scrollTop: $indicatorLast.offset().top - 200
            }, 500);
        });
    });
});
