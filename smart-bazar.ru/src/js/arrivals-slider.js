/* Arrivals Slider */
(function() {
    let $arrivalsSlider = new Swiper('[data-slider="arrivals"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.arrivals__next',
            prevEl: '.arrivals__prev',
        }
    });
})();