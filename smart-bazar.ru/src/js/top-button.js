  /* Button to top positioning */
  $(function() {
    var buttonToTop = $('[data-component="top-button"]');

    if (!buttonToTop) return false;

    var Toffset = 250;
    var Boffset = $('[data-component="footer"]').outerHeight();

    $(window).scroll(function(){
      if ($(this).scrollTop() > Toffset){
        buttonToTop.addClass("active");
      } else {
        buttonToTop.removeClass("active");
      }
    });
  });