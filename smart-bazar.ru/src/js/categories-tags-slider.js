/* Categories Tags Slider */
$(document).ready(function(){
    let $categoriesTagsSlider = $('[data-slider="categories-tags"]');

    if ($categoriesTagsSlider.length === 0) {
        return false;
    }

    $categoriesTagsSlider.slick({
        mobileFirst: true,
        infinite: false,
        slidesPerRow: 1,
        rows: 4,
        dots: true,
        prevArrow: $('.categories__tags-prev'),
        nextArrow: $('.categories__tags-next'),
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    dots: false,
                    autoSlidesToShow: true,
                    variableWidth: true,
                    slidesPerRow: 4,
                    rows: 2,
                }
            }
        ]
    });

    function isInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function resizeSlider() {
        let winWidth = $(window).width();
        let $categoriesTagsSliderList  = $categoriesTagsSlider.find('.slick-list');

        if (winWidth >= bpMD) {
            let $categoriesTagsSliderPrev  = $('.categories__tags-prev');
            let $categoriesTagsSliderNext  = $('.categories__tags-next');
            let $categoriesTagsSliderSlide = $categoriesTagsSlider.find('.slick-slide');
            let sliderListWidth,
                slideWidth,
                leftOffset;

            sliderListWidth = $categoriesTagsSliderList.width();
            slideWidth = $categoriesTagsSliderSlide.width();

            if (isInViewport($categoriesTagsSliderSlide[0])) {
                $categoriesTagsSliderPrev[0].style.setProperty('display', 'none', 'important');
                $categoriesTagsSliderNext[0].style.setProperty('display', 'none', 'important');
            } else {
                $categoriesTagsSliderPrev[0].style.setProperty('display', '');
                $categoriesTagsSliderNext[0].style.setProperty('display', '');
            }

            $categoriesTagsSliderList.css('left', 0);

            $categoriesTagsSliderNext.on('click', function(e) {
                leftOffset = slideWidth - sliderListWidth;
                $categoriesTagsSliderList.css("left", '-'+ leftOffset + 'px');
            });

            $categoriesTagsSliderPrev.on('click', function(e) {
                $categoriesTagsSliderList.css("left", 0);
            });
        } else {
            $categoriesTagsSliderList.css('left', '');
        }
    }

    resizeSlider();

    $(window).on('orientationchange resize', function() {
        $categoriesTagsSlider.slick('resize');
        resizeSlider();
    });
});