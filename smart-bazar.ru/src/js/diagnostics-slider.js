/* Diagnostics Slider */
(function() {
    let $diagnosticsSlider = new Swiper('[data-slider="diagnostics"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.diagnostics__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.diagnostics__next',
            prevEl: '.diagnostics__prev',
        }
    });
})();