/* Search */
$(document).ready(function(){
    let $search = $('[data-component="search"]');
    let $searchInput = $('[data-element="search-input"]');
    let $searchClear = $('[data-element="search-clear"]');

    $searchInput.on('input', function(e) {
        $search.fadeIn();
        $search.addClass(activeClass);
    });

    $searchClear.on('click', function(e) {
        e.preventDefault();

        $searchInput.val('');
        $search.removeClass(activeClass);
    });

    /*$(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            $searchContent.hide();
        }
    });

    $(document).on('click', function(e) {
        if ($searchInput.has(e.target).length === 0 && !$searchInput.is(e.target)) {
            $searchContent.hide();
        }
    });*/
});
