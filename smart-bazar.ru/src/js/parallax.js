/* Parallax */
(function() {
    const parallaxScenes = document.querySelectorAll('[data-component="parallax"]');

    if (!parallaxScenes) {
        return false;
    }

    for (const scene of parallaxScenes) {
        let parallaxInstance = new Parallax(scene);
    }
})();