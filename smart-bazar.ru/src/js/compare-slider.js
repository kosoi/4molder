/* Compare slider */
$(document).ready(function(){
    let $compareSlider = $('[data-slider="compare"]');
    let $compareCounter = $('[data-element="compare-counter"]');

    $compareSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        var i = (currentSlide ? currentSlide : 0) + 1;
        $compareCounter.text(i + ' из ' + slick.slideCount);
    });

    $compareSlider.slick({
        infinite: false,
        autoSlidesToShow: true,
        variableWidth: true,
        asNavFor: '[data-slider="compare-props"]',
        prevArrow: $('.compare__slider-prev'),
        nextArrow: $('.compare__slider-next'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                }
            }
        ]
    });

    let $comparePropsSlider = $('[data-slider="compare-props"]');
    $comparePropsSlider.slick({
        infinite: false,
        autoSlidesToShow: true,
        variableWidth: true,
        draggable: false,
        touchMove: false,
        swipe: false,
        arrows: false,
        asNavFor: '[data-slider="compare"]',
    });

    $(window).on('orientationchange resize', function() {
        $compareSlider.slick('resize');
        $comparePropsSlider.slick('resize');
    });
});