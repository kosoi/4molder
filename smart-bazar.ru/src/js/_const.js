'use strict';

const activeClass  =   'is-active';
const fixedClass   =   'is-fixed';
const focusClass   =   'is-focused';
const hoverClass   =   'is-hover';
const disabledClass =  'is-disabled';
const visibleClass =   'is-visible';
const expandedClass =  'is-expanded';
const selectedClass =  'is-selected';
const collapsedClass = 'is-collapsed';
const scrolledClass =  'is-scrolled';
const headerFixedClass = 'header-is-fixed';
const lockedScrollClass = 'scroll-is-locked';
const searchVisibleClass = 'search-is-visible';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';
const asideVisibleClass = 'aside-is-visible';

const $header = $('[data-component="header"]');
const $body = $('.body');
const $main = $('.main');

const bpSM = 520;
const bpMD = 768;
const bpLG = 1024;
const bpXL = 1280;
const bp2Xl = 1440;
const bp3XL = 1920;