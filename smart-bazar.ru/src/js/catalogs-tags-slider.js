/* Catalog Tags Slider */
(function() {
    let $catalogTagsSlider = new Swiper('[data-slider="catalog-tags"]', {
        slidesPerView: 'auto',
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
})();