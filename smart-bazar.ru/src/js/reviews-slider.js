/* Reviews Slider */
(function() {
    let $reviewsSlider = new Swiper('[data-slider="reviews"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.reviews__next',
            prevEl: '.reviews__prev',
        }
    });
})();