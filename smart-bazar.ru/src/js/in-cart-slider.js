/* In cart Slider */
//(function() {
    let $inCartSlider = new Swiper('[data-slider="in-cart"]', {
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.in-cart__slider-pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                slidesPerColumn: 2,
                slidesPerColumnFill: 'row',
                spaceBetween: 8
            },
            1280: {
                slidesPerView: 3,
                slidesPerColumn: 1,
                spaceBetween: 24
            },
        },
    });
//})();