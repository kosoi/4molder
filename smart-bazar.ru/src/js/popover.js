/* Popover */
tippy('[data-element="popover-trigger"]', {
    maxWidth: 333,
    theme: 'light',
    offset: [0, 21],
    allowHTML: true,
    interactive: true,
});

tippy('[data-element="popover-template-trigger"]', {
    maxWidth: 333,
    theme: 'light',
    offset: [-56, 21],
    allowHTML: true,
    appendTo: () => document.body,
    interactive: true,
    content(reference) {
        const id = reference.getAttribute('data-template');
        const template = document.getElementById(id);
        return template.innerHTML;
    },
});