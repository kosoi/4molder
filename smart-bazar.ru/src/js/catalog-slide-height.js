/* Details with equal name width */
$(document).ready(function() {
    catalogSliderAdjustment();
    $(window).on('orientationchange resize', function() {
        catalogSliderAdjustment();
    });

    function catalogSliderAdjustment() {
        let $catalogSlider = $('[data-slider="catalog"]');

        $catalogSlider.each(function (index, item) {
            let $slider = $(item);
            let $slides = $slider.find('[data-element="catalog-slide"]');
            let $slidesCards = $slider.find('[data-element="catalog-slide"] .item-card');

            let tallestSlide = Math.max.apply(Math, $slidesCards.map(function(index, el) {
                return $(el)[0].scrollHeight;
            }));

            $slides.css('height', tallestSlide + 'px');

        });
    };
});