/* Brands Slider */
(function() {
    let $brandsSlider = new Swiper('[data-slider="brands"]', {
        slidesPerView: 4,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.brands__pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
            },
            480: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
                //slidesPerColumn: 2,
                //slidesPerColumnFill: 'row',
            },
            1280: {
                slidesPerView: 4,
            },
        },
    });
})();