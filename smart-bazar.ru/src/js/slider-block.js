/* Slider Block Slider */
(function() {
    let $sliderBlockSlider = new Swiper('[data-slider="slider-block"]', {
        slidesPerView: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.slider-block__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.slider-block__next',
            prevEl: '.slider-block__prev',
        }
    });
})();