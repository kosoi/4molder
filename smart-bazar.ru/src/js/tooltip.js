/* Tooltip */
tippy('[data-element="tooltip-trigger"]', {
    maxWidth: 324,
    theme: 'light',
    offset: [0, 21],
    interactive: true,
});
