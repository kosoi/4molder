/* Panel on mobile, tabs on desktop */
$(document).ready(function(){
    let $panelTabsToggle = $('[data-component="panel-tabs"] [data-element="panel-tabs-toggle"]');
    let $panelTabs = $('[data-component="panel-tabs"]');

    $panelTabsToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleId = $toggle.attr('href');
        let $component = $toggle.closest('[data-component="panel-tabs"]');
        let $tabActive = $component.find('[data-element="panel-tabs-tab"][data-id="' + toggleId.slice(1) +'"]');
        let $tabs = $component.find('[data-element="panel-tabs-tab"]');
        let $toggles = $component.find('[data-element="panel-tabs-toggle"]');
        let $indicator = $toggle.closest('.has-indicator');
        let $faqItems = $tabActive.find('[data-element="accordion-item"]');
        let windowWidth = $(window).width();

        if ($toggle.hasClass(activeClass)) {
            $toggle.removeClass(activeClass);
            $tabActive.removeClass(activeClass);

            if (windowWidth < bpLG) {
                $tabActive.hide();
            }

            if ($indicator.length > 0) {
                $indicator.addClass(disabledClass).removeClass(selectedClass);
            }
        } else {
            $tabs.removeClass(activeClass);
            $toggles.removeClass(activeClass);
            $tabActive.addClass(activeClass);
            $toggle.addClass(activeClass);

            if (windowWidth < bpLG) {
                $tabs.hide();
                $tabActive.slideDown(300);
            }

            if ($indicator.length > 0) {
                $indicator.removeClass(disabledClass).removeClass(selectedClass);
            }
        }

        if ($indicator.length > 0) {
            $indicator.siblings().removeClass(selectedClass).addClass(disabledClass);
        }

        if ($faqItems.length > 0) {
            $faqItems.addClass(collapsedClass);
            $faqItems.find('[data-element="accordion-content"]').hide();
            $faqItems.find('[data-component="accordion"]').addClass(disabledClass);
        }
    });

    panelTabsResponsive();
    $(window).on('orientationchange resize', function() {
      panelTabsResponsive();
    });

    function panelTabsResponsive() {
        $panelTabs.each(function (index, item) {

            let $tabs = $(item);
            let $toggles = $tabs.find('[data-element="panel-tabs-toggle"]');
            let $content = $tabs.find('[data-element="panel-tabs-content"]');

            $toggles.each(function (index, toggle) {
                let $toggle = $(toggle);
                let toggleId = $toggle.attr('href');
                let $tabItem = $toggle.closest('[data-element="panel-tabs-item"]');
                let $tab = $tabs.find('[data-element="panel-tabs-tab"][data-id="' + toggleId.slice(1) +'"]');

                if ($(window).width() < bpLG) {
                    $tab.appendTo($tabItem);
                } else {
                    $tab.appendTo($content);
                    $tab.show();
                }
            });
        });
    }
});