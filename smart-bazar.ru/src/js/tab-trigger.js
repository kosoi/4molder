/* Scroll to tabs item */
$(document).ready(function(){
    let $tabTrigger = $('[data-element="tab-trigger"]');

    $tabTrigger.on('click', function(e){
        e.preventDefault();

        let $trigger = $(this);

        let triggerId = $trigger.attr('href');
        let $tabActive = $('[data-element="tabs-tab"][data-id="'+ triggerId.slice(1) +'"]');
        let $toggle = $('[data-element="tabs-toggle"][href="'+ triggerId +'"]');
        let $tabs = $toggle.closest('[data-component="tabs"]');

        if (!($toggle.hasClass(activeClass))) {
            $tabs.find('[data-element="tabs-tab"]').removeClass(activeClass);
            $tabs.find('[data-element="tabs-toggle"]').removeClass(activeClass);
            $tabs.find($tabActive).addClass(activeClass);
            $toggle.addClass(activeClass);
        }

        $('html, body').animate({
            scrollTop: $tabs.offset().top - 200
        }, 500);
    });
});