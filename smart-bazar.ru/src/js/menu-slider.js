/* Menu Slider */
$(document).ready(function(){
    var $menuSlider = $('[data-slider="menu"]');

    $menuSlider.slick({
        mobileFirst: true,
        arrows: false,
        dots: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 699,
                settings: {
                    slidesToShow: 2,
                } 
            },
            {
                breakpoint: 767,
                settings: "unslick"
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $menuSlider.slick('resize');
    });
});
