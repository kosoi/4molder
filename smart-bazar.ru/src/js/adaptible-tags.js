/*
  Consider using these polyfills to broaden browser support:
    — https://www.npmjs.com/package/classlist-polyfill
    — https://www.npmjs.com/package/nodelist-foreach-polyfill
*/
let $adaptibleBlocks = document.querySelectorAll('[data-component="adaptible-tags"]');

Array.from($adaptibleBlocks).forEach(function(container) {
    let containerId = container.dataset.id;
    let containerBreakpoint = +container.dataset.breakpoint;
    let windowWidth = window.innerWidth;

    if (windowWidth >= containerBreakpoint) {
        return false;
    }

    let tagClass = container.dataset.tagClass;
    let tippyPlacement = container.dataset.tippyPlacement;
    let primary = container;
    let primaryItems = primary.querySelectorAll('[data-element="adaptible-item"]');
    container.classList.add("--jsfied");

    // insert "more" button and duplicate the list
    primary.insertAdjacentHTML(
    "beforeend",
    `
      <li class="ta__item col" data-element="adaptible-more">
        <button class="${tagClass}" data-template="adaptible-tags-template-${containerId}" data-element="popover-template-trigger" data-tippy-placement="${tippyPlacement}" data-tippy-maxWidth="335">&hellip;</button>
        <div hidden>
          <div id="adaptible-tags-template-${containerId}">
            <ul class="tags tags--default row row--xs" data-element="adaptible-secondary">
              ${primary.innerHTML}
            </ul>
          </div>
        </div>
      </li>
    `);

    let secondary = container.querySelector('[data-element="adaptible-secondary"]');
    let secondaryItems = secondary.querySelectorAll('li');
    let allItems = container.querySelectorAll('[data-element="adaptible-item"]');
    let moreElement = primary.querySelector('[data-element="adaptible-more"]');

    let secondaryTags = secondary.querySelectorAll('.tag');
    secondaryTags.forEach((tag, i) => {
        tag.classList.remove('tag--sm');
    });

    // adapt tabs

    const doAdapt = () => {
        // reveal all items for the calculation
        allItems.forEach(item => {
            item.classList.remove("hidden");
        });

        // hide items that won't fit in the Primary
        let stopWidth = moreElement.offsetWidth;
        let hiddenItems = [];
        const primaryWidth = primary.offsetWidth;
        primaryItems.forEach((item, i) => {
            if (primaryWidth >= stopWidth + item.offsetWidth) {
                stopWidth += item.offsetWidth;
            } else {
                item.classList.add("hidden");
                hiddenItems.push(i);
            }
        });

        // toggle the visibility of More button and items in Secondary
        if (!hiddenItems.length) {
            moreElement.classList.add("hidden");
        } else {
            secondaryItems.forEach((item, i) => {
                if (!hiddenItems.includes(i)) {
                    item.classList.add("hidden");
                }
            });
        }
    };

    doAdapt(); // adapt immediately on load
    window.addEventListener("resize", doAdapt); // adapt on window resize
    window.addEventListener("orientationchange", doAdapt); // adapt on window resize
});