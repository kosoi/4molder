/* Categories Slider */
(function() {
    let $categoriesSlider = new Swiper('[data-slider="categories"]', {
        slidesPerView: 5,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.categories__menu-pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            576: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 3,
            },
            1024: {
                slidesPerView: 4,
            },
            1280: {
                slidesPerView: 5,
            }
        }
    });
})();