/* Catalog Slider */
(function() {
    let $catalogSliders = Array.from(document.querySelectorAll('[data-slider="catalog"]'));

    $catalogSliders.forEach((slider) => {
        let nextButton = slider.closest('.catalog').querySelector('.swiper-button-next');
        let prevButton = slider.closest('.catalog').querySelector('.swiper-button-prev');

        let $catalogSlider = new Swiper(slider, {
            slidesPerView: 'auto',
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
        });
    });
})();