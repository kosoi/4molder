/* Product slider */
//$(function() {
    let $productThumbs = new Swiper('[data-slider="product-thumbs"]', {
        spaceBetween: 12,
        slidesPerView: 3,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });

    let $productSlider = new Swiper('[data-slider="product"]', {
        navigation: {
            nextEl: '.product__slider-next',
            prevEl: '.product__slider-prev',
        },
        thumbs: {
            swiper: $productThumbs,
        },
    });
    //});