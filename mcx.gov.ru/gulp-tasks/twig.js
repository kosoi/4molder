'use strict';

let twig = require('gulp-twig'),
    fs = require('fs'),
    prettyHtml = require('gulp-pretty-html');


module.exports = function () {
    var data = this.opts.src.templates.data || {};
    return this.gulp.src(this.opts.src.templates.path + "/" + this.opts.argv.view)
        .pipe(twig({
            data: JSON.parse(fs.readFileSync(data)),
            functions: [
                {
                    name: 'assets',
                    func: function (url) {
                        return ((data.assets || '') + url).replace(/\/+/g, '/').replace(/^\//, '');
                    }
                }
            ],
        }))
        .pipe(prettyHtml())
        .pipe(this.gulp.dest(this.opts.dist));
};
