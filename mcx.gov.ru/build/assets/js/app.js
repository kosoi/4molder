'use strict';

/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');
    const activeClass = 'is-active';

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $this = $(this);
        let $item = $this.closest('[data-element="accordion-item"]');
        let $itemContent = $item.find('[data-element="accordion-content"]');

        if ($item.hasClass(activeClass)) {
            $itemContent.slideUp();
            $item.removeClass(activeClass);
        } else {
            $item.siblings().removeClass(activeClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp();
            $itemContent.slideDown();
            $item.addClass(activeClass);
        }
    });
});

/* Banners slider */
$(document).ready(function(){
    let $bannersSlider = $('[data-component="banners-slider"]');

    $bannersSlider.slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(window).on('orientationchange resize', function() {
        $bannersSlider.slick('resize');
    });
});
/* Date picker */
$(document).ready(function(){
    moment.locale('ru');
    let start = moment();
    let $datepickerInput = $('[data-element="datepicker"]');
    let picker = $datepickerInput.daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
        startDate: start,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10) + 1,
        locale: {
            "format": "DD MMMM YYYY",
            "separator": " - ",
            "daysOfWeek": [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            "monthNames": [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Aвгуст",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
            "firstDay": 1
        }
    });

    $datepickerInput.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD MMMM YYYY'));
    });

    $datepickerInput.on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
$(document).ready(function() {
    let $document = $('[data-component="document"]');
    let $documentBody = $('[data-element="document-body"]');
    let $documentAside = $('[data-element="document-aside"]');
    let ps;

    ps = new PerfectScrollbar('[data-element="document-body"]', {
        useBothWheelAxes: false,
        suppressScrollX: true
    });

    function documentScroll() {
        let documentAsidePadding = $documentAside.innerHeight() - $documentAside.height();
        let documentAsideHeight = 0;

        if ($(window).width() >= 1024) {
            $documentAside.children().each(function(){
                documentAsideHeight = documentAsideHeight + $(this).outerHeight(true);
            });

            $documentBody.css('height', documentAsideHeight + documentAsidePadding);

            if (!$document.length) {
                return false;
            }
        } else {
            $documentBody.css('height', '');
            if (ps) {
                ps.destroy();
                ps = null;
            }
        }
    }

    documentScroll();

    $(window).on('orientationchange resize', function() {
        documentScroll();
    });
});
/* Dropdown */
$(document).ready(function(){
    let $dropdowns = $('[data-component="dropdown"]');
    let $dropdownToggles = $('[data-element="dropdown-toggle"]');
    const openClass = 'is-open';

    $dropdownToggles.click(function(e) {
        let toggle = $(this);
        let component = toggle.closest('[data-component="dropdown"]');

        hideDropdowns($dropdownToggles.not(this).closest('[data-component="dropdown"]'));

        component.toggleClass(openClass);
    });

    function hideDropdowns(dropdowns) {
        dropdowns.removeClass(openClass);
    }

    $(document).keyup(function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideDropdowns($dropdowns);
        }
    });

    $(document).on('click', function (e) {
      if (!e.target.closest('[data-component="dropdown"]')) {
          hideDropdowns($dropdowns);
      }
    });
});
// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Поле обязательно к заполнению",
    required:       "Поле обязательно к заполнению",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"], [data-element="select-xs"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    $(function() {
        $('[data-validate="parsley"]').parsley({
            excluded: "[disabled], :hidden",
            errorClass: 'has-error',
            successClass: 'has-success',
            errorsWrapper: '<div class="form__errors-list text-sm"></div>',
            errorTemplate: '<div class="form__error"></div>',
            errorsContainer (field) {
                return field.$element.closest('.form__group').find('.form__errors');
            },
            classHandler (field) {
                const $parent = field.$element.closest('.form__group');
                if ($parent.length) return $parent;

                return $parent;
            }
        });
    });

});

/* Inputbox */
$(document).ready(function() {
    let $inputbox = $('[data-element="inputbox"]');
    let $inputboxControl = $('[data-element="inputbox"] input');

    $inputbox.on('click', function(evt) {
        let control = $(this);
        control.addClass('focus');
    });

    $inputboxControl.on('blur', function(evt) {
        let input = $(this);
        input.closest('[data-element="inputbox"]').removeClass('focus');
    });
});

/* Masked input */
$(document).ready(function(){
    $(":input[data-inputmask]").inputmask();

    /* Masked input fix For parsley validation */
    $(document).on('keypress', function(evt) {
        if(evt.isDefaultPrevented()) {
            // Assume that's because of maskedInput
            // See https://github.com/guillaumepotier/Parsley.js/issues/1076
            $(evt.target).trigger('input');
        }
    });
});
/* Measures slider */
$(document).ready(function(){
    let $measuresSlider = $('[data-component="measures-slider"]');

    $measuresSlider.slick({
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(window).on('orientationchange resize', function() {
        $measuresSlider.slick('resize');
    });
});
/* Measures slider */
$(document).ready(function(){
    let $measuresWidgetSlider = $('[data-component="measures-widget-slider"]');

    $measuresWidgetSlider.slick({
        dots: true
    });

    $(window).on('orientationchange resize', function() {
        $measuresWidgetSlider.slick('resize');
    });
});
/* Menu */
$(document).ready(function(){
    let $menu = $('[data-component="menu"]');
    let $menuToggle = $('[data-element="menu-toggle"]');
    const activeClass = 'is-active';

    $menuToggle.on('click', function(e) {
        e.preventDefault();

        $(this).toggleClass(activeClass);
        $menu.slideToggle();
    });
});

/* Modal */
$(document).ready(function(){
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'mfp-zoom-in'
    });
});

/* Navbar on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $navbar = document.querySelector('[data-component="navbar"]');
    const stickyClass = 'is-sticky';

    if (!$navbar) {
        return false;
    }

    // Fallback for old browsers
    let onScroll = function () {
        $navbar.classList.toggle(stickyClass, window.scrollY > 0);
    };

    onScroll();
    window.addEventListener('scroll', function() {
        onScroll();
    });
});
/* Popover */
$(document).ready(function(){
    const popovers = tippy('[data-element="popover-toggle"]', {
        maxWidth: 324,
        theme: 'light',
        distance: 14,
        arrow: false,
        allowHTML: true,
        trigger: 'click',
        interactive: true,
        appendTo: () => document.body,
    });
});

$(document).ready(function() {
    let $rangeSlider = $('[data-element="range-slider-element"]');

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range-slider"]');
        let $rangeInput = $rangeComponent.find('[data-element="range-slider-input"]');
        let instance;

        $range.ionRangeSlider({
            skin: 'round',
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            onStart: function(data) {
                $rangeInput.prop("value", data.from);
            },
            onChange: function(data) {
                $rangeInput.prop("value", data.from);
            }
        });

        instance = $range.data("ionRangeSlider");

        $rangeInput.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val < instance.result.min) {
                val = instance.result.min;
            } else if (val > instance.result.max) {
                val = instance.result.max;
            }

            instance.update({
                from: val
            });
        });
    });
});
$(document).ready(function() {
    let perfectScrollbarOptions = {};
    let ps;

    let $selectXs = $('[data-element="select-xs"]');
    selet2Init($selectXs, 'select-xs text-sm');

    let $selectHeader = $('[data-element="select-header"]');
    selet2Init($selectHeader, 'header-select select-xs text-sm', 'placement');

    let $selectLocation = $('[data-element="select-location"]');
    selet2Init($selectLocation, 'header-select select-xs select-location text-sm');

    let $select = $('[data-element="select"]');
    selet2Init($select);

    /* Select2 */
    function selet2Init(select, customClass) {
        select
            .select2({
                dropdownCssClass: customClass,
                containerCssClass: customClass,
                minimumResultsForSearch: Infinity,
                width: '100%'
            })
            .on('select2:open', function () {
                // setTimeout is required here as we need to wait for all DOM
                // operations carried out by Select2.
                setTimeout(() => {
                    ps = new PerfectScrollbar('.select2-results__options', perfectScrollbarOptions);

                    // Re-initializing perfect-scrollbar is required on keyup and input
                    // as Select2 clears everything inside '.select2-results__options'
                    // and reset its content on those events.
                }, 0);
            })
            .on('select2:closing', function () {
                ps.destroy();
            });
    }
});
/* Show all elements */
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) {
        this.text(t2);
    } else {
        this.text(t1);
    }
    return this;
};

$(document).ready(function(){
    let $showAllToggle = $('[data-element="show-all-toggle"]');

    $showAllToggle.on('click', function(e) {
        e.preventDefault();

        let toggle = $(this);
        let toggleIcon = toggle.find('[data-element="show-all-toggle-icon"]');
        let toggleIconClassOn = toggleIcon.attr('data-class-on');
        let toggleIconClassOff = toggleIcon.attr('data-class-off');
        let toggleText = toggle.find('[data-element="show-all-toggle-text"]');
        let toggleTextOn = toggleText.attr('data-text-on');
        let toggleTextOff = toggleText.attr('data-text-off');
        let toggleNumber = toggle.find('[data-element="show-all-toggle-number"]');
        let showAllComponent = toggle.closest('[data-component="show-all-section"]');

        toggleIcon.toggleClass(toggleIconClassOn);
        toggleIcon.toggleClass(toggleIconClassOff);
        toggleText.toggleText(toggleTextOn, toggleTextOff);
        toggleNumber.fadeToggle();

        showAllComponent.find('[data-element="hidden-element"]').toggleClass('hidden');
    });
});

/* Show all elements */
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) {
        this.text(t2);
    } else {
        this.text(t1);
    }
    return this;
};

$(document).ready(function(){
    let $showClippedToggle = $('[data-element="show-clipped-toggle"]');
    let unclippedClass = 'is-unclipped';

    $showClippedToggle.on('click', function(e) {
        e.preventDefault();

        let toggle = $(this);
        let toggleIcon = toggle.find('[data-element="show-clipped-toggle-icon"]');
        let toggleIconClassOn = toggleIcon.attr('data-class-on');
        let toggleIconClassOff = toggleIcon.attr('data-class-off');
        let toggleText = toggle.find('[data-element="show-clipped-toggle-text"]');
        let toggleTextOn = toggleText.attr('data-text-on');
        let toggleTextOff = toggleText.attr('data-text-off');
        let clippedElement = toggle.parent().siblings('[data-element="element-clipped"]');

        toggleIcon.toggleClass(toggleIconClassOn);
        toggleIcon.toggleClass(toggleIconClassOff);
        toggleText.toggleText(toggleTextOn, toggleTextOff);

        clippedElement.toggleClass(unclippedClass);
    });
});

$(document).ready(function() {
    let $tableWrapper = $('.table-wrapper');

    if (!$tableWrapper.length) {
        return false;
    }

    let ps = new PerfectScrollbar('.table-wrapper', {
        useBothWheelAxes: false,
        suppressScrollY: true
    });
});
/* Tabs */
$(document).ready(function(){
    let $tabs = $('[data-component="tabs"]');
    let $tabToggle = $('[data-component="tabs"] .tabs__link');
    let activeClass = 'is-active';

    $tabToggle.on('click', function(e) {
        e.preventDefault();

        let $this = $(this);
        let tabID = $this.attr('href');
        let tabElement = '[data-id="' + tabID.slice(1) +'"]';
        let $tabs = $this.closest('[data-component="tabs"]');

        if (!($this.hasClass(activeClass))) {
            $tabs.find('.tabs__tab').removeClass(activeClass);
            $tabs.find('.tabs__link.' + activeClass).removeClass(activeClass);
            $tabs.find(tabElement).addClass(activeClass);
            $this.addClass(activeClass);
        }
    });
});

/* Tooltip */
$(document).ready(function(){
    const tooltips = tippy('[data-element="tooltip-trigger"]', {
        maxWidth: 192,
        distance: 14,
        arrow: false,
        theme: 'dark'
    });
});