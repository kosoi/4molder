$(document).ready(function() {
    let $document = $('[data-component="document"]');
    let $documentBody = $('[data-element="document-body"]');
    let $documentAside = $('[data-element="document-aside"]');
    let ps;

    ps = new PerfectScrollbar('[data-element="document-body"]', {
        useBothWheelAxes: false,
        suppressScrollX: true
    });

    function documentScroll() {
        let documentAsidePadding = $documentAside.innerHeight() - $documentAside.height();
        let documentAsideHeight = 0;

        if ($(window).width() >= 1024) {
            $documentAside.children().each(function(){
                documentAsideHeight = documentAsideHeight + $(this).outerHeight(true);
            });

            $documentBody.css('height', documentAsideHeight + documentAsidePadding);

            if (!$document.length) {
                return false;
            }
        } else {
            $documentBody.css('height', '');
            if (ps) {
                ps.destroy();
                ps = null;
            }
        }
    }

    documentScroll();

    $(window).on('orientationchange resize', function() {
        documentScroll();
    });
});