/* Tooltip */
$(document).ready(function(){
    const tooltips = tippy('[data-element="tooltip-trigger"]', {
        maxWidth: 192,
        distance: 14,
        arrow: false,
        theme: 'dark'
    });
});