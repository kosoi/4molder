/* Masked input */
$(document).ready(function(){
    $(":input[data-inputmask]").inputmask();

    /* Masked input fix For parsley validation */
    $(document).on('keypress', function(evt) {
        if(evt.isDefaultPrevented()) {
            // Assume that's because of maskedInput
            // See https://github.com/guillaumepotier/Parsley.js/issues/1076
            $(evt.target).trigger('input');
        }
    });
});