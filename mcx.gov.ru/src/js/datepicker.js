/* Date picker */
$(document).ready(function(){
    moment.locale('ru');
    let start = moment();
    let $datepickerInput = $('[data-element="datepicker"]');
    let picker = $datepickerInput.daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
        startDate: start,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10) + 1,
        locale: {
            "format": "DD MMMM YYYY",
            "separator": " - ",
            "daysOfWeek": [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            "monthNames": [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Aвгуст",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
            "firstDay": 1
        }
    });

    $datepickerInput.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD MMMM YYYY'));
    });

    $datepickerInput.on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});