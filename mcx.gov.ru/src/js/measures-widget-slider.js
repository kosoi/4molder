/* Measures slider */
$(document).ready(function(){
    let $measuresWidgetSlider = $('[data-component="measures-widget-slider"]');

    $measuresWidgetSlider.slick({
        dots: true
    });

    $(window).on('orientationchange resize', function() {
        $measuresWidgetSlider.slick('resize');
    });
});