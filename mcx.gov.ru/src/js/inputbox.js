/* Inputbox */
$(document).ready(function() {
    let $inputbox = $('[data-element="inputbox"]');
    let $inputboxControl = $('[data-element="inputbox"] input');

    $inputbox.on('click', function(evt) {
        let control = $(this);
        control.addClass('focus');
    });

    $inputboxControl.on('blur', function(evt) {
        let input = $(this);
        input.closest('[data-element="inputbox"]').removeClass('focus');
    });
});
