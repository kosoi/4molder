/* Show all elements */
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) {
        this.text(t2);
    } else {
        this.text(t1);
    }
    return this;
};

$(document).ready(function(){
    let $showClippedToggle = $('[data-element="show-clipped-toggle"]');
    let unclippedClass = 'is-unclipped';

    $showClippedToggle.on('click', function(e) {
        e.preventDefault();

        let toggle = $(this);
        let toggleIcon = toggle.find('[data-element="show-clipped-toggle-icon"]');
        let toggleIconClassOn = toggleIcon.attr('data-class-on');
        let toggleIconClassOff = toggleIcon.attr('data-class-off');
        let toggleText = toggle.find('[data-element="show-clipped-toggle-text"]');
        let toggleTextOn = toggleText.attr('data-text-on');
        let toggleTextOff = toggleText.attr('data-text-off');
        let clippedElement = toggle.parent().siblings('[data-element="element-clipped"]');

        toggleIcon.toggleClass(toggleIconClassOn);
        toggleIcon.toggleClass(toggleIconClassOff);
        toggleText.toggleText(toggleTextOn, toggleTextOff);

        clippedElement.toggleClass(unclippedClass);
    });
});
