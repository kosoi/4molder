$(document).ready(function() {
    let $rangeSlider = $('[data-element="range-slider-element"]');

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range-slider"]');
        let $rangeInput = $rangeComponent.find('[data-element="range-slider-input"]');
        let instance;

        $range.ionRangeSlider({
            skin: 'round',
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            onStart: function(data) {
                $rangeInput.prop("value", data.from);
            },
            onChange: function(data) {
                $rangeInput.prop("value", data.from);
            }
        });

        instance = $range.data("ionRangeSlider");

        $rangeInput.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val < instance.result.min) {
                val = instance.result.min;
            } else if (val > instance.result.max) {
                val = instance.result.max;
            }

            instance.update({
                from: val
            });
        });
    });
});