$(document).ready(function() {
    let perfectScrollbarOptions = {};
    let ps;

    let $selectXs = $('[data-element="select-xs"]');
    selet2Init($selectXs, 'select-xs text-sm');

    let $selectHeader = $('[data-element="select-header"]');
    selet2Init($selectHeader, 'header-select select-xs text-sm', 'placement');

    let $selectLocation = $('[data-element="select-location"]');
    selet2Init($selectLocation, 'header-select select-xs select-location text-sm');

    let $select = $('[data-element="select"]');
    selet2Init($select);

    /* Select2 */
    function selet2Init(select, customClass) {
        select
            .select2({
                dropdownCssClass: customClass,
                containerCssClass: customClass,
                minimumResultsForSearch: Infinity,
                width: '100%'
            })
            .on('select2:open', function () {
                // setTimeout is required here as we need to wait for all DOM
                // operations carried out by Select2.
                setTimeout(() => {
                    ps = new PerfectScrollbar('.select2-results__options', perfectScrollbarOptions);

                    // Re-initializing perfect-scrollbar is required on keyup and input
                    // as Select2 clears everything inside '.select2-results__options'
                    // and reset its content on those events.
                }, 0);
            })
            .on('select2:closing', function () {
                ps.destroy();
            });
    }
});