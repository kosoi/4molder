/* Show all elements */
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) {
        this.text(t2);
    } else {
        this.text(t1);
    }
    return this;
};

$(document).ready(function(){
    let $showAllToggle = $('[data-element="show-all-toggle"]');

    $showAllToggle.on('click', function(e) {
        e.preventDefault();

        let toggle = $(this);
        let toggleIcon = toggle.find('[data-element="show-all-toggle-icon"]');
        let toggleIconClassOn = toggleIcon.attr('data-class-on');
        let toggleIconClassOff = toggleIcon.attr('data-class-off');
        let toggleText = toggle.find('[data-element="show-all-toggle-text"]');
        let toggleTextOn = toggleText.attr('data-text-on');
        let toggleTextOff = toggleText.attr('data-text-off');
        let toggleNumber = toggle.find('[data-element="show-all-toggle-number"]');
        let showAllComponent = toggle.closest('[data-component="show-all-section"]');

        toggleIcon.toggleClass(toggleIconClassOn);
        toggleIcon.toggleClass(toggleIconClassOff);
        toggleText.toggleText(toggleTextOn, toggleTextOff);
        toggleNumber.fadeToggle();

        showAllComponent.find('[data-element="hidden-element"]').toggleClass('hidden');
    });
});
