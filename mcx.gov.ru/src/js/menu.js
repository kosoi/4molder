/* Menu */
$(document).ready(function(){
    let $menu = $('[data-component="menu"]');
    let $menuToggle = $('[data-element="menu-toggle"]');
    const activeClass = 'is-active';

    $menuToggle.on('click', function(e) {
        e.preventDefault();

        $(this).toggleClass(activeClass);
        $menu.slideToggle();
    });
});
