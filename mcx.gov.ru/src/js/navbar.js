/* Navbar on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $navbar = document.querySelector('[data-component="navbar"]');
    const stickyClass = 'is-sticky';

    if (!$navbar) {
        return false;
    }

    // Fallback for old browsers
    let onScroll = function () {
        $navbar.classList.toggle(stickyClass, window.scrollY > 0);
    };

    onScroll();
    window.addEventListener('scroll', function() {
        onScroll();
    });
});