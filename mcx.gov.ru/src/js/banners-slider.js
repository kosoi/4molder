/* Banners slider */
$(document).ready(function(){
    let $bannersSlider = $('[data-component="banners-slider"]');

    $bannersSlider.slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(window).on('orientationchange resize', function() {
        $bannersSlider.slick('resize');
    });
});