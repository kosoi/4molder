/* Modal */
$(document).ready(function(){
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'mfp-zoom-in'
    });
});
