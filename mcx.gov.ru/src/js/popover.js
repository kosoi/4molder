/* Popover */
$(document).ready(function(){
    const popovers = tippy('[data-element="popover-toggle"]', {
        maxWidth: 324,
        theme: 'light',
        distance: 14,
        arrow: false,
        allowHTML: true,
        trigger: 'click',
        interactive: true,
        appendTo: () => document.body,
    });
});
