'use strict';

/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');
    const activeClass = 'is-active';

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $this = $(this);
        let $item = $this.closest('[data-element="accordion-item"]');
        let $itemContent = $item.find('[data-element="accordion-content"]');

        if ($item.hasClass(activeClass)) {
            $itemContent.slideUp();
            $item.removeClass(activeClass);
        } else {
            $item.siblings().removeClass(activeClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp();
            $itemContent.slideDown();
            $item.addClass(activeClass);
        }
    });
});
