/* Dropdown */
$(document).ready(function(){
    let $dropdowns = $('[data-component="dropdown"]');
    let $dropdownToggles = $('[data-element="dropdown-toggle"]');
    const openClass = 'is-open';

    $dropdownToggles.click(function(e) {
        let toggle = $(this);
        let component = toggle.closest('[data-component="dropdown"]');

        hideDropdowns($dropdownToggles.not(this).closest('[data-component="dropdown"]'));

        component.toggleClass(openClass);
    });

    function hideDropdowns(dropdowns) {
        dropdowns.removeClass(openClass);
    }

    $(document).keyup(function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideDropdowns($dropdowns);
        }
    });

    $(document).on('click', function (e) {
      if (!e.target.closest('[data-component="dropdown"]')) {
          hideDropdowns($dropdowns);
      }
    });
});