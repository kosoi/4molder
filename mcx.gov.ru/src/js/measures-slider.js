/* Measures slider */
$(document).ready(function(){
    let $measuresSlider = $('[data-component="measures-slider"]');

    $measuresSlider.slick({
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(window).on('orientationchange resize', function() {
        $measuresSlider.slick('resize');
    });
});