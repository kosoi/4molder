$(document).ready(function() {
    let $tableWrapper = $('.table-wrapper');

    if (!$tableWrapper.length) {
        return false;
    }

    let ps = new PerfectScrollbar('.table-wrapper', {
        useBothWheelAxes: false,
        suppressScrollY: true
    });
});