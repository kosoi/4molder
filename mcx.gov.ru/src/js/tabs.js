/* Tabs */
$(document).ready(function(){
    let $tabs = $('[data-component="tabs"]');
    let $tabToggle = $('[data-component="tabs"] .tabs__link');
    let activeClass = 'is-active';

    $tabToggle.on('click', function(e) {
        e.preventDefault();

        let $this = $(this);
        let tabID = $this.attr('href');
        let tabElement = '[data-id="' + tabID.slice(1) +'"]';
        let $tabs = $this.closest('[data-component="tabs"]');

        if (!($this.hasClass(activeClass))) {
            $tabs.find('.tabs__tab').removeClass(activeClass);
            $tabs.find('.tabs__link.' + activeClass).removeClass(activeClass);
            $tabs.find(tabElement).addClass(activeClass);
            $this.addClass(activeClass);
        }
    });
});
